<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date, java.util.Locale, java.text.SimpleDateFormat" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/StringTransform.tld" prefix="strt" %>
<%! int counter=0; %>
<html>
    <head>
      <meta charset="UTF-8">
    </head>
    <body>
        <h2>Index!</h2>
        <a href="./hello">Hello</a>
        <a href="./TestJpa">TestJpa</a>
        <p>cette page à été visitée un total de <b><%= ++counter %></b> fois.</p>
        <% SimpleDateFormat frenchFormat = new SimpleDateFormat("EEE MMM dd hh:mm:ss yyyy", Locale.FRANCE); %>
        <p>la date est: <b><%= frenchFormat.format(new Date()) %></b></p>
        <img src="./resources/500x500.png"/>
        <p>ce texte est à l'endroit</p>
        <p>${strt:reverse("ce texte est à l'envers")}</p>
        <h3>boucle for jstl</h3>
        <c:forEach var="i" begin="0" end="10" step="1">
            <p>n°<c:out value="${ i }" /> !</p>
        </c:forEach>
    </body>
</html>

