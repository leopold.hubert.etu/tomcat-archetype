package ${groupId};

import ${groupId}.entities.Professor;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestJpa extends HttpServlet {

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException {
        try {
            res.getWriter().println("professeurs (avec JPA)\n\n");
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("test-jpa");
            EntityManager em = emf.createEntityManager();

            Professor client = new Professor();
            client.setNom("Mathieu");
            client.setPrenom("Philippe");
            em.getTransaction().begin();
            em.persist(client);
            em.getTransaction().commit();

            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Professor> cq = cb.createQuery(Professor.class);
            Root<Professor> rootEntry = cq.from(Professor.class);
            CriteriaQuery<Professor> all = cq.select(rootEntry);

            TypedQuery<Professor> allQuery = em.createQuery(all);

            for(Professor p:allQuery.getResultList())
                res.getWriter().println(String.join(", ", String.valueOf(p.getId()), p.getNom(), p.getPrenom())+"\n");
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

}
