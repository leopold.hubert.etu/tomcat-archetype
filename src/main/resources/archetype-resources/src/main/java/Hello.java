package ${groupId};

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Hello extends HttpServlet {

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("<html><head>");
            sb.append("<meta charset=\"UTF-8\">");
            sb.append("</head><body>");
            sb.append("<h1>HELLO WORLD!</h1>");
            sb.append("<h2>vos headers:</h1>");
            req.getHeaderNames().asIterator()
                    .forEachRemaining((headerName -> sb.append(headerName+"="+req.getHeader(headerName)+"<br/>")));
            sb.append("<h2>context variables</h2>");
            sb.append("context.param.key"+getInitParameter("context.param.key")+"<br>");
            sb.append("</body></html>");
            res.getWriter().println(sb.toString());
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

}
