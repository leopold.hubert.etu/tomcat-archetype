package ${groupId}.tld;

public class StringTransform {

    public static String reverse(String in) {
        return new StringBuilder(in).reverse().toString();
    }

}
